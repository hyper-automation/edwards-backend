package main.java.com.edwards;

import com.quadwave.common.exception.ErrorMessageException;
import com.quadwave.nbt.common.axpflow.command.SetWorkflowVariableCommand;
import com.quadwave.nbt.common.execution.entity.WorkflowInstance;
import com.quadwave.nbt.common.execution.entity.WorkflowNode;
import com.quadwave.nbt.common.execution.entity.WorkflowVariable;
import com.quadwave.nbt.common.execution.java.JavaNodeHandler;
import com.quadwave.nbt.common.json.JacksonJsonUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ConvertJsonToList extends JavaNodeHandler {
    @Override
    public void execute(WorkflowInstance workflowInstance, WorkflowNode workflowNode) {
        try{
            SetWorkflowVariableCommand setWorkflowVariableCommand = new SetWorkflowVariableCommand();
            List<WorkflowVariable> workflowVariables = new ArrayList<>();

            WorkflowVariable finalResultVar = workflowInstance.getVariableByName("finalResultForm");
            WorkflowVariable getSideAVar=workflowInstance.getVariableByName("getSideA");
            WorkflowVariable getSideBVar=workflowInstance.getVariableByName("getSideB");
            WorkflowVariable getSideCVar=workflowInstance.getVariableByName("getSideC");
            WorkflowVariable getSideDVar=workflowInstance.getVariableByName("getSideD");
            WorkflowVariable getSideEVar=workflowInstance.getVariableByName("getSideE");
            WorkflowVariable getSideFVar=workflowInstance.getVariableByName("getSideF");

            WorkflowVariable SideAVar=workflowInstance.getVariableByName("side1");
            WorkflowVariable SideBVar=workflowInstance.getVariableByName("side2");
            WorkflowVariable SideCVar=workflowInstance.getVariableByName("side3");
            WorkflowVariable SideDVar=workflowInstance.getVariableByName("side4");
            WorkflowVariable SideEVar=workflowInstance.getVariableByName("side5");
            WorkflowVariable SideFVar=workflowInstance.getVariableByName("side6");

            if (finalResultVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + finalResultVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getSideAVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getSideAVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getSideBVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getSideBVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getSideCVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getSideCVar + " not-found");
                ex.printStackTrace();
                throw ex;
            } if (getSideDVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getSideDVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getSideEVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getSideEVar + " not-found");
                ex.printStackTrace();
                throw ex;
            } if (getSideFVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getSideFVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            JSONArray finalResultVarList=new JSONArray(finalResultVar.getValue());
            System.out.println("**********"+finalResultVarList+"***************");
            int length=finalResultVarList.length();
            List<Side> sidesAList  = new ArrayList();
            List<Side> sidesBList  = new ArrayList();
            List<Side> sidesCList  = new ArrayList();
            List<Side> sidesDList  = new ArrayList();
            List<Side> sidesEList  = new ArrayList();
            List<Side> sidesFList  = new ArrayList();
            if(!(finalResultVarList.getJSONObject(0).has("finalResult"))){
                System.out.println("*************** To false block *************************");
                JSONArray side1JsonArray=finalResultVarList;
                for (int i = 0; i < side1JsonArray.length(); i++) {
                    JSONObject jsonObject=side1JsonArray.getJSONObject(i);
                    SideAVar.setValue("Side "+jsonObject.getString("side"));
                    sidesAList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
                }
                sidesAList.sort(Comparator.comparing(a->Integer.parseInt(a.getId()),Comparator.naturalOrder()));
                getSideAVar.setDataType("getSideA");
                getSideAVar.setName("getSideA");
                getSideAVar.setValue(JacksonJsonUtil.getInlineJson(sidesAList));
                getSideAVar.setCollection(true);
            }else {
                JSONArray side1JsonArray = length > 0 ? new JSONArray(finalResultVarList.getJSONObject(0).getString("finalResult")) : new JSONArray();

                for (int i = 0; i < side1JsonArray.length(); i++) {
                    JSONObject jsonObject = side1JsonArray.getJSONObject(i);
                    SideAVar.setValue("Side "+jsonObject.getString("side"));
                    sidesAList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")), jsonObject.getString("value"), jsonObject.getString("side")));
                }
                sidesAList.sort(Comparator.comparing(a -> Integer.parseInt(a.getId()), Comparator.naturalOrder()));
                System.out.println(sidesAList + "----------------" + SideAVar.getValue());
                getSideAVar.setDataType("getSideA");
                getSideAVar.setName("getSideA");
                getSideAVar.setValue(JacksonJsonUtil.getInlineJson(sidesAList));
                getSideAVar.setCollection(true);
                JSONArray side2JsonArray = length > 1 ? new JSONArray(finalResultVarList.getJSONObject(1).getString("finalResult")) : new JSONArray();
                for (int i = 0; i < side2JsonArray.length(); i++) {
                    JSONObject jsonObject = side2JsonArray.getJSONObject(i);
                    SideBVar.setValue("Side "+jsonObject.getString("side"));
                    sidesBList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")), jsonObject.getString("value"), jsonObject.getString("side")));
                }
                sidesBList.sort(Comparator.comparing(a -> Integer.parseInt(a.getId()), Comparator.naturalOrder()));
                getSideBVar.setDataType("getSideB");
                getSideBVar.setName("getSideB");
                getSideBVar.setValue(JacksonJsonUtil.getInlineJson(sidesBList));
                getSideBVar.setCollection(true);
                JSONArray side3JsonArray = length > 2 ? new JSONArray(finalResultVarList.getJSONObject(2).getString("finalResult")) : new JSONArray();
                for (int i = 0; i < side3JsonArray.length(); i++) {
                    JSONObject jsonObject = side3JsonArray.getJSONObject(i);
                    SideCVar.setValue("Side "+jsonObject.getString("side"));
                    sidesCList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")), jsonObject.getString("value"), jsonObject.getString("side")));
                }
                sidesCList.sort(Comparator.comparing(a -> Integer.parseInt(a.getId()), Comparator.naturalOrder()));
                getSideCVar.setDataType("getSideC");
                getSideCVar.setName("getSideC");
                getSideCVar.setValue(JacksonJsonUtil.getInlineJson(sidesCList));
                getSideCVar.setCollection(true);
                JSONArray side4JsonArray = length > 3 ? new JSONArray(finalResultVarList.getJSONObject(3).getString("finalResult")) : new JSONArray();
                for (int i = 0; i < side4JsonArray.length(); i++) {
                    JSONObject jsonObject = side4JsonArray.getJSONObject(i);
                    SideDVar.setValue("Side "+jsonObject.getString("side"));
                    sidesDList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")), jsonObject.getString("value"), jsonObject.getString("side")));
                }
                sidesDList.sort(Comparator.comparing(a -> Integer.parseInt(a.getId()), Comparator.naturalOrder()));
                getSideDVar.setDataType("getSideD");
                getSideDVar.setName("getSideD");
                getSideDVar.setValue(JacksonJsonUtil.getInlineJson(sidesDList));
                getSideDVar.setCollection(true);
                JSONArray side5JsonArray = length > 4 ? new JSONArray(finalResultVarList.getJSONObject(4).getString("finalResult")) : new JSONArray();
                for (int i = 0; i < side5JsonArray.length(); i++) {
                    JSONObject jsonObject = side5JsonArray.getJSONObject(i);
                    SideEVar.setValue("Side "+jsonObject.getString("side"));
                    sidesEList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")), jsonObject.getString("value"), jsonObject.getString("side")));
                }
                sidesEList.sort(Comparator.comparing(a -> Integer.parseInt(a.getId()), Comparator.naturalOrder()));
                getSideEVar.setDataType("getSideE");
                getSideEVar.setName("getSideE");
                getSideEVar.setValue(JacksonJsonUtil.getInlineJson(sidesEList));
                getSideEVar.setCollection(true);
                JSONArray side6JsonArray = length > 5 ? new JSONArray(finalResultVarList.getJSONObject(5).getString("finalResult")) : new JSONArray();
                for (int i = 0; i < side6JsonArray.length(); i++) {
                    JSONObject jsonObject = side6JsonArray.getJSONObject(i);
                    SideFVar.setValue("Side "+jsonObject.getString("side"));
                    sidesFList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")), jsonObject.getString("value"), jsonObject.getString("side")));
                }
                sidesFList.sort(Comparator.comparing(a -> Integer.parseInt(a.getId()), Comparator.naturalOrder()));
                getSideFVar.setDataType("getSideF");
                getSideFVar.setName("getSideF");
                getSideFVar.setValue(JacksonJsonUtil.getInlineJson(sidesFList));
                getSideFVar.setCollection(true);
            }

            setWorkflowVariableCommand.setInstanceId(workflowInstance.getId());
            workflowVariables.add(getSideAVar);
            workflowVariables.add(getSideBVar);
            workflowVariables.add(getSideCVar);
            workflowVariables.add(getSideDVar);
            workflowVariables.add(getSideEVar);
            workflowVariables.add(getSideFVar);
            workflowVariables.add(SideAVar);
            workflowVariables.add(SideBVar);
            workflowVariables.add(SideCVar);
            workflowVariables.add(SideDVar);
            workflowVariables.add(SideEVar);
            workflowVariables.add(SideFVar);

            setWorkflowVariableCommand.setWorkflowVariables(workflowVariables);
            setWorkflowVariableCommand.execute();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
       // String js="[{\"side\":\"1\",\"id\":742,\"value\":\"1\",\"sampleNumber\":1},{\"side\":\"1\",\"id\":753,\"value\":\"1.11\",\"sampleNumber\":4},{\"side\":\"1\",\"id\":754,\"value\":\"0.2\",\"sampleNumber\":2},{\"side\":\"1\",\"id\":755,\"value\":\"0.3\",\"sampleNumber\":3}]";
        String js="[{\"finalResult\":\"[{\\\"id\\\": 742, \\\"side\\\": \\\"1\\\", \\\"value\\\": \\\"1\\\", \\\"sampleNumber\\\": 1}, {\\\"id\\\": 753, \\\"side\\\": \\\"1\\\", \\\"value\\\": \\\"1.11\\\", \\\"sampleNumber\\\": 4}, {\\\"id\\\": 754, \\\"side\\\": \\\"1\\\", \\\"value\\\": \\\"0.2\\\", \\\"sampleNumber\\\": 2}, {\\\"id\\\": 755, \\\"side\\\": \\\"1\\\", \\\"value\\\": \\\"0.3\\\", \\\"sampleNumber\\\": 3}]\"},{\"finalResult\":\"[{\\\"id\\\": 757, \\\"side\\\": \\\"2\\\", \\\"value\\\": \\\"0.1\\\", \\\"sampleNumber\\\": 1}]\"}]";
        JSONArray jsonArray=new JSONArray(js);
  //      System.out.println(jsonArray.getJSONObject(0).getString("finalResult"));
        System.out.println(jsonArray.getJSONObject(0).has("finalResult"));
    }
}
