package main.java.com.edwards;

import com.quadwave.nbt.common.axpflow.command.SetWorkflowVariableCommand;
import com.quadwave.nbt.common.axpflow.service.Uploader;
import com.quadwave.nbt.common.execution.entity.WorkflowInstance;
import com.quadwave.nbt.common.execution.entity.WorkflowNode;
import com.quadwave.nbt.common.execution.entity.WorkflowVariable;
import com.quadwave.nbt.common.execution.java.JavaNodeHandler;
import com.quadwave.nbt.common.json.JacksonJsonUtil;
import com.quadwave.nbt.common.vo.DocumentInfoVo;
import com.quadwave.nbt.pojo.general.configuration.Dms;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.util.Matrix;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddPageNumbers extends JavaNodeHandler {
    @Override
    public void execute(WorkflowInstance workflowInstance, WorkflowNode workflowNode) {
        try {
            System.out.println("*****************add page numbers*******************************");
            List<WorkflowVariable> workflowVariables = new ArrayList<>();
            SetWorkflowVariableCommand setWorkflowVariableCommand = new SetWorkflowVariableCommand();
            WorkflowVariable finalDocWorkflow = new WorkflowVariable();
            WorkflowVariable testcase = workflowInstance.getVariableByName("testcaseNo");
            WorkflowVariable reportversion = workflowInstance.getVariableByName("reportversion");
            WorkflowVariable version = workflowInstance.getVariableByName("version");
            JSONObject jsonObject=new JSONObject(reportversion.getValue());
            String testCaseValue=testcase.getValue();
            String versionValue=jsonObject.getString("version");

            String filepath="/home/eiquser/WebappServer/platform-data/Edwards/MergedReports/merged.pdf";
            //String filepath="/Org_54904/fianelFile.pdf";
            String outputPath="/home/eiquser/WebappServer/platform-data/Edwards/MergedReports/"+testCaseValue+"_report_"+versionValue.toUpperCase()+".pdf";
            System.out.println("---------------------------"+filepath);
            File oldFile=new File(filepath);
            System.out.println(oldFile.getAbsolutePath()+"------------------------");
            doIt(filepath,"test",outputPath , testCaseValue);
            System.out.println("new "+outputPath);
            File finalFile=new File(outputPath);
            List<File> fileList=new ArrayList<>();
            System.out.println(fileList);
            fileList.add(finalFile);
            System.out.println("------------------------------------"+fileList.size());
            Dms defaultDmsName = executionContext.getGeneralService().getDmsService().findDefault(workflowInstance.getOrganizationId());
            Uploader doUploader = executionContext.getDocumentService().newUploader(workflowInstance.getOrganizationId());
            String uploadedFile = doUploader.uploadFile("/Org_"+workflowInstance.getOrganizationId()+"/report/",
                    outputPath, defaultDmsName.getName());//dmspath , localpath , dms name;
            System.out.println(uploadedFile);
            List<DocumentInfoVo> docInfo = new ArrayList<>();
            DocumentInfoVo documentInfoVo = new DocumentInfoVo();
            documentInfoVo.setUrl(uploadedFile);
            documentInfoVo.setName(finalFile.getName());
            documentInfoVo.setLocalPath(finalFile.getAbsolutePath());
            docInfo.add(documentInfoVo);

            String variableValue = JacksonJsonUtil.getInlineJson(docInfo);
            finalDocWorkflow.setDataType("Document");
            finalDocWorkflow.setValue(variableValue);
            finalDocWorkflow.setName("finalDoc");
            finalDocWorkflow.setCollection(false);

            //   _log.info("file path from ftp location" + result);


            setWorkflowVariableCommand.setInstanceId(workflowInstance.getId());
            workflowVariables.add(finalDocWorkflow);


            setWorkflowVariableCommand.setWorkflowVariables(workflowVariables);
            setWorkflowVariableCommand.execute();

            System.out.println("-----------------test executed---------------");

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
       // System.out.println( AddPageNumbers.addPageNumber("src\\main\\resources\\pdfsequence\\MergeFile_12.pdf"));
    AddPageNumbers.doIt("C:\\Users\\chandipriya.g\\Downloads\\fianelFile (3).pdf","text","C:\\Users\\chandipriya.g\\Downloads\\output.pdf", "1234");
    }
    public static void doIt( String file, String message, String  outfile , String testCaseValue ) throws IOException
    {
        System.out.println("----started --page numbers----------");
        File file1=new File(file);
        System.out.println(file1.getAbsoluteFile()+"***********************");
        try
        {
            System.out.println("----in try block----------");
            PDDocument doc=PDDocument.load(file1);
            //PDDocument doc = Loader.loadPDF(file1);
            // new PDDocument(MemoryUsageSetting.setupTempFileOnly());
           // doc.save(file1);
            System.out.println("____________________");
            System.out.println("----in try block----------");
            PDFont font = PDType1Font.HELVETICA_BOLD;
            float fontSize = 6.0f;
            int count=doc.getNumberOfPages();
            System.out.println("************************"+count);
            int i=1;
            for( PDPage page : doc.getPages() )
            {
                System.out.println("inside loop-----------------"+count);
                PDRectangle pageSize = page.getMediaBox();
                float stringWidth = font.getStringWidth( message )*fontSize/1000f;
                // calculate to center of the page
                int rotation = page.getRotation();
                boolean rotate = rotation == 90 || rotation == 270;
                float pageWidth = rotate ? pageSize.getHeight() : pageSize.getWidth();
                float pageHeight = rotate ? pageSize.getWidth() : pageSize.getHeight();


                // append the content to the existing stream
                try (PDPageContentStream contentStream = new PDPageContentStream(doc, page, PDPageContentStream.AppendMode.APPEND, true, true))
                {
                    contentStream.beginText();
                    // set font and font size
                    contentStream.setFont( font, fontSize );
                    // set text color to red
                    contentStream.setNonStrokingColor(0, 0, 1);

                    contentStream.setTextMatrix(Matrix.getTranslateInstance(pageWidth-90, pageHeight-40));

                    contentStream.showText("Test "+testCaseValue+" Page "+i+" of "+count);
                    contentStream.endText();
                }
                i++;
            }

            doc.save(outfile);
            doc.close();
        }catch (Exception e){
            System.out.println(e.getMessage()+"**********************");
        }

    }

}
