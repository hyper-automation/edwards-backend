package main.java.com.edwards;


import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.quadwave.common.exception.ErrorMessageException;
import com.quadwave.nbt.common.axpflow.command.SetWorkflowVariableCommand;
import com.quadwave.nbt.common.axpflow.service.Uploader;
import com.quadwave.nbt.common.execution.entity.WorkflowInstance;
import com.quadwave.nbt.common.execution.entity.WorkflowNode;
import com.quadwave.nbt.common.execution.entity.WorkflowVariable;
import com.quadwave.nbt.common.execution.java.JavaNodeHandler;
import com.quadwave.nbt.common.json.JacksonJsonUtil;
import com.quadwave.nbt.common.vo.DocumentInfoVo;
import com.quadwave.nbt.pojo.common.ErrorMessage;
import com.quadwave.nbt.pojo.general.configuration.Dms;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PdfMergerCondAndAging extends JavaNodeHandler {

    private static Logger _log = Logger.getLogger(PdfMergerCondAndAging.class);
    @Override
    public void execute(WorkflowInstance workflowInstance, WorkflowNode workflowNode) {
        _log.info("Pdf File Merging");
        // Read variable properties
        try {

            List<WorkflowVariable> workflowVariables = new ArrayList<>();
            SetWorkflowVariableCommand setWorkflowVariableCommand = new SetWorkflowVariableCommand();
           WorkflowVariable finalDocWorkflow =new WorkflowVariable();
            List<InputStream> inputPdfList = new ArrayList<InputStream>();
            WorkflowVariable wrksheetVar=workflowInstance.getVariableByName("docvar");
            WorkflowVariable C1var = workflowInstance.getVariableByName("C1Doc");
            WorkflowVariable C2var = workflowInstance.getVariableByName("C2Doc");
            WorkflowVariable AAvar = workflowInstance.getVariableByName("conditionagingAA");
            System.out.println("----------------------------------start-------------");
            if (wrksheetVar == null) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + C1var + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (C1var == null) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + C1var + " not-found");
                ex.printStackTrace();
               throw ex;
            }
            if (AAvar == null ) {

                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + AAvar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (C2var == null ) {

                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + C2var + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            System.out.println("----------------------------------start 2-------------");

            _log.info("c1var"+C1var.getValue());
            _log.info("c2var"+C2var.getValue());
            _log.info("AAvar"+AAvar.getValue());
            String wrksheetVarValue = wrksheetVar.getValue();
            if (wrksheetVarValue != null && wrksheetVarValue.trim().length() > 0 && !(wrksheetVarValue.equals("[]")))
            {
                DocumentInfoVo docs = JacksonJsonUtil.getObject(wrksheetVarValue, DocumentInfoVo.class);
                File file = new File(docs.getLocalPath());
                try {
                    inputPdfList.add(new FileInputStream(file));
                } catch (Exception e) {
                    _log.info("file not found");
                }
            }
            String C1Value = C1var.getValue();
                if (C1Value != null && C1Value.trim().length() > 0&&!(C1Value.equals("[]")))
                {
                    DocumentInfoVo docs = JacksonJsonUtil.getObject(C1Value, DocumentInfoVo.class);
                    File file = new File(docs.getLocalPath());
                    try {
                        inputPdfList.add(new FileInputStream(file));
                    } catch (Exception e) {
                        _log.info("file not found");
                    }
                }


                String C2Value = C2var.getValue();
              if(C2Value != null && C2Value.trim().length() > 0&&!(C2Value.equals("[]"))) {
               DocumentInfoVo docs = JacksonJsonUtil.getObject(C2Value, DocumentInfoVo.class);
                File file = new File(docs.getLocalPath());
                try {
                    inputPdfList.add(new FileInputStream(file));
                }catch (Exception e){
                    _log.info("file not found");
                }
            }


                String AAValue = AAvar.getValue();
                if(C1Value != null && C1Value.trim().length() > 0&&!(C1Value.equals("[]"))) {
                    DocumentInfoVo docs = JacksonJsonUtil.getObject(AAValue, DocumentInfoVo.class);
                    File file = new File(docs.getLocalPath());
                    try {
                        inputPdfList.add(new FileInputStream(file));
                    } catch (Exception e) {
                        _log.info("file not found");
                    }
                }

            _log.info("-----------------------------------------------------");
            System.out.println("-----------------------------------------------------");
            String filepath="/home/eiquser/WebappServer/platform-data/Edwards/MergedReports/"+"fianelFile.pdf";
            Dms defaultDmsName = executionContext.getGeneralService().getDmsService().findDefault(workflowInstance.getOrganizationId());
            File finalFile=new File(filepath);
            _log.info("-----------------------------------------------------");
            mergePdfFiles(inputPdfList,new FileOutputStream(finalFile));
            List<File> fileList=new ArrayList<>();
            _log.info(fileList);
            fileList.add(finalFile);
            Uploader doUploader = executionContext.getDocumentService().newUploader(workflowInstance.getOrganizationId());
            String uploadedFile = doUploader.uploadFile("/Org_"+workflowInstance.getOrganizationId()+"/report/",
                    filepath, defaultDmsName.getName());//dmspath , localpath , dms name;
            _log.info(uploadedFile);
            List<DocumentInfoVo> docInfo = new ArrayList<>();
                DocumentInfoVo documentInfoVo = new DocumentInfoVo();
                documentInfoVo.setUrl(uploadedFile);
                documentInfoVo.setName(finalFile.getName());
                documentInfoVo.setLocalPath(finalFile.getAbsolutePath());
                docInfo.add(documentInfoVo);


            String variableValue = JacksonJsonUtil.getInlineJson(docInfo);
            finalDocWorkflow.setDataType("Document");
            finalDocWorkflow.setValue(variableValue);
            finalDocWorkflow.setName("finalDoc");
            finalDocWorkflow.setCollection(false);

            //   _log.info("file path from ftp location" + result);


            setWorkflowVariableCommand.setInstanceId(workflowInstance.getId());
            workflowVariables.add(finalDocWorkflow);


                setWorkflowVariableCommand.setWorkflowVariables(workflowVariables);
                setWorkflowVariableCommand.execute();

            System.out.println("-----------------test executed---------------");

        } catch(Throwable e){
            e.printStackTrace();

            ErrorMessage errorMessage = new ErrorMessage("Cannot upload to DMS");
            errorMessage.addMessage("errors-while-uploading-file-to-dms");
            errorMessage.addMessage(e.getMessage());

            ErrorMessageException ex = new ErrorMessageException(errorMessage);
            throw ex;
        }
    }
    void mergePdfFiles(List<InputStream> inputPdfList,
                       OutputStream outputStream) throws Exception {
        //Create document and pdfReader objects.
        Document document = new Document();
        List<PdfReader> readers =
                new ArrayList<PdfReader>();
        int totalPages = 0;
        if(inputPdfList.size()>0) {
            //Create pdf Iterator object using inputPdfList.
            Iterator<InputStream> pdfIterator = inputPdfList.iterator();

            // Create reader list for the input pdf files.
            while (pdfIterator.hasNext()) {
                InputStream pdf = pdfIterator.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages = totalPages + pdfReader.getNumberOfPages();
            }

            // Create writer for the outputStream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);

            //Open document.
            document.open();

            //Contain the pdf data.
            PdfContentByte pageContentByte = writer.getDirectContent();

            PdfImportedPage pdfImportedPage;
            int currentPdfReaderPage = 1;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();

            // Iterate and process the reader list.
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                //Create page and add content.
                while (currentPdfReaderPage <= pdfReader.getNumberOfPages()) {
                    document.newPage();
                    pdfImportedPage = writer.getImportedPage(
                            pdfReader, currentPdfReaderPage);
                    pageContentByte.addTemplate(pdfImportedPage, 0, 0);
                    currentPdfReaderPage++;
                }
                //          addPage(pdfReader, new ByteArrayOutputStream(),i,10);
                currentPdfReaderPage = 1;
            }
        }


        //Close document and outputStream.
        outputStream.flush();
        document.close();
        outputStream.close();

        System.out.println("Pdf files merged successfully.");
    }
}
