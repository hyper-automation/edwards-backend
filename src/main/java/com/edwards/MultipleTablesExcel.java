package main.java.com.edwards;

import com.fasterxml.jackson.core.type.TypeReference;
import com.quadwave.common.exception.ErrorMessageException;
import com.quadwave.nbt.common.axpflow.command.SetWorkflowVariableCommand;
import com.quadwave.nbt.common.axpflow.service.Uploader;
import com.quadwave.nbt.common.execution.entity.WorkflowInstance;
import com.quadwave.nbt.common.execution.entity.WorkflowNode;
import com.quadwave.nbt.common.execution.entity.WorkflowVariable;
import com.quadwave.nbt.common.execution.java.JavaNodeHandler;
import com.quadwave.nbt.common.json.JacksonJsonUtil;
import com.quadwave.nbt.common.vo.DocumentInfoVo;
import com.quadwave.nbt.pojo.general.configuration.Dms;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.poi.hssf.record.ExtendedFormatRecord.CENTER;

public class MultipleTablesExcel extends JavaNodeHandler {
    public void execute(WorkflowInstance workflowInstance, WorkflowNode workflowNode) {
        try{
            List<WorkflowVariable> workflowVariables = new ArrayList<>();
            SetWorkflowVariableCommand setWorkflowVariableCommand = new SetWorkflowVariableCommand();

            WorkflowVariable finalDocWorkflow = new WorkflowVariable();
            WorkflowVariable datVar=new WorkflowVariable();
            // Get Variables from platform
            WorkflowVariable getNarrowestVar = workflowInstance.getVariableByName("getNarrowest");
            WorkflowVariable finalResultVar = workflowInstance.getVariableByName("finalResultForm");


            WorkflowVariable getVariableValueSideVar = workflowInstance.getVariableByName("getVariableValueSide");

            WorkflowVariable getOtherMagnaValueVar = workflowInstance.getVariableByName("getOtherMagnaValue");
            WorkflowVariable GetPackageTestInfoVar = workflowInstance.getVariableByName("GetPackageTestInfo");
            WorkflowVariable getPostCondVar = workflowInstance.getVariableByName("getPostCond");
            WorkflowVariable getPreCondVar = workflowInstance.getVariableByName("getPreCond");
            WorkflowVariable getTorqueVar = workflowInstance.getVariableByName("getTorque");

            // Verify variables are null or not
            if (getNarrowestVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getNarrowestVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (finalResultVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + finalResultVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }

            if (getVariableValueSideVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getVariableValueSideVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getOtherMagnaValueVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getOtherMagnaValueVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (GetPackageTestInfoVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + GetPackageTestInfoVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getPostCondVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getPostCondVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            if (getPreCondVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getPreCondVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }

            if (getTorqueVar == null ) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + getTorqueVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }

            // Get the values from the variables

            TypeReference<List<Side>> typeRef = new TypeReference<List<Side>>() {
            };

            List<Side> norrowestList  = JacksonJsonUtil.getObject(getNarrowestVar.getValue(), typeRef);
            List<Side> sidesAList  = new ArrayList<>();
            List<Side> sidesBList  = new ArrayList<>();
            List<Side> sidesCList  = new ArrayList<>();
            List<Side> sidesDList  = new ArrayList<>();
            List<Side> sidesEList  = new ArrayList<>();
            List<Side> sidesFList  = new ArrayList<>();

            JSONArray finalResultVarList=new JSONArray(finalResultVar.getValue());
            String finalResultVarvalue=finalResultVar.getValue();
            int length=finalResultVarList.length();
           /* System.out.println(finalResultVarvalue);
            System.out.println(finalResultVarList.get(0));*/
            JSONArray side1JsonArray=length>0?new JSONArray(finalResultVarList.getJSONObject(0).getString("finalResult")):new JSONArray();
            for (int i = 0; i < side1JsonArray.length(); i++) {
                JSONObject jsonObject=side1JsonArray.getJSONObject(i);
                sidesAList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
            }
            JSONArray side2JsonArray=length>1?new JSONArray(finalResultVarList.getJSONObject(1).getString("finalResult")):new JSONArray();
            for (int i = 0; i < side2JsonArray.length(); i++) {
                JSONObject jsonObject=side2JsonArray.getJSONObject(i);
                sidesBList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
            }
            JSONArray side3JsonArray=length>2?new JSONArray(finalResultVarList.getJSONObject(2).getString("finalResult")):new JSONArray();
            for (int i = 0; i < side3JsonArray.length(); i++) {
                JSONObject jsonObject=side3JsonArray.getJSONObject(i);
                sidesCList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
            }
            JSONArray side4JsonArray=length>3?new JSONArray(finalResultVarList.getJSONObject(3).getString("finalResult")):new JSONArray();
            for (int i = 0; i < side4JsonArray.length(); i++) {
                JSONObject jsonObject=side4JsonArray.getJSONObject(i);
                sidesDList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
            }
            JSONArray side5JsonArray=length>4?new JSONArray(finalResultVarList.getJSONObject(4).getString("finalResult")):new JSONArray();
            for (int i = 0; i < side5JsonArray.length(); i++) {
                JSONObject jsonObject=side5JsonArray.getJSONObject(i);
                sidesEList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
            }
            JSONArray side6JsonArray=length>5?new JSONArray(finalResultVarList.getJSONObject(5).getString("finalResult")):new JSONArray();
            for (int i = 0; i < side6JsonArray.length(); i++) {
                JSONObject jsonObject=side6JsonArray.getJSONObject(i);
                sidesFList.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
            }

            JSONArray variableValueSideList  = new JSONArray(getVariableValueSideVar.getValue());

            JSONArray otherMagna=new JSONArray(getOtherMagnaValueVar.getValue());
            JSONObject packageTestInfo=new JSONObject(GetPackageTestInfoVar.getValue());
            JSONArray postCond=new JSONArray(getPostCondVar.getValue());
            JSONArray preCond=new JSONArray(getPreCondVar.getValue());
            JSONArray torque=new JSONArray(getTorqueVar.getValue());


            WorkflowVariable testCaseNoVar = workflowInstance.getVariableByName("testCaseNo");
            WorkflowVariable testCodeVar = workflowInstance.getVariableByName("testCode");
            WorkflowVariable reportPath = workflowInstance.getVariableByName("reportPath");


            String testCaseNo=testCaseNoVar.getValue();
            String testCode=testCodeVar.getValue();
            String filePath=reportPath.getValue()+testCaseNo+"_"+testCode+".xlsx";
            File file=new File(filePath);

            String testItem=packageTestInfo.getString("testItem");
            int isNarrowest= Integer.parseInt(String.valueOf(packageTestInfo.get("isNarrowest")));
            int sideForSample= Integer.parseInt(StringUtils.isEmpty(String.valueOf(packageTestInfo.get("sidesPerSample")))?"0":String.valueOf(packageTestInfo.get("sidesPerSample")));

            // Process excel reort based on the testcode
            processExcelReport(file,variableValueSideList,otherMagna,torque,preCond,postCond,testCaseNo,testItem,isNarrowest,testCode,sideForSample,sidesAList, sidesBList, sidesCList, sidesDList, sidesEList, sidesFList,norrowestList);

            //Upload generated excel file to DMS
            Dms defaultDmsName = executionContext.getGeneralService().getDmsService().findDefault(workflowInstance.getOrganizationId());
            Uploader doUploader = executionContext.getDocumentService().newUploader(workflowInstance.getOrganizationId());
            String uploadedFile = doUploader.uploadFile("/Org_" + workflowInstance.getOrganizationId() + "/report/",
                    filePath, defaultDmsName.getName());

            // Create document variable and add the value send to platform
            List<DocumentInfoVo> docInfo = new ArrayList<>();
            DocumentInfoVo documentInfoVo = new DocumentInfoVo();
            documentInfoVo.setUrl(uploadedFile);
            documentInfoVo.setName(file.getName());
            documentInfoVo.setLocalPath(file.getAbsolutePath());
            docInfo.add(documentInfoVo);

            String variableValue = JacksonJsonUtil.getInlineJson(docInfo);
            finalDocWorkflow.setDataType("Document");
            finalDocWorkflow.setValue(variableValue);
            finalDocWorkflow.setName("excelDoc");
            finalDocWorkflow.setCollection(false);

            datVar.setDataType("String");
            datVar.setValue(String.valueOf(finalResultVarvalue));
            datVar.setName("result");
            datVar.setCollection(false);

            workflowVariables.add(datVar);

            setWorkflowVariableCommand.setInstanceId(workflowInstance.getId());
            workflowVariables.add(finalDocWorkflow);


            setWorkflowVariableCommand.setWorkflowVariables(workflowVariables);
            setWorkflowVariableCommand.execute();


        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        String testCode="Seal Width Measurement";
        int sideForSample=3;
        int isNarrowest=0;
        String testCaseNo="12345";
        String testItem="Tray";
        List<Side> listA = new ArrayList<>();
        List<Side> listB = new ArrayList<>();
        List<Side> listC = new ArrayList<>();
        List<Side> listD = new ArrayList<>();
        List<Side> listE = new ArrayList<>();
        List<Side> listF = new ArrayList<>();
        List<Side> narrowestList=new ArrayList();
        String finalResult="[{\"finalResult\":\"[{\\\"id\\\": 532, \\\"side\\\": \\\"a\\\", \\\"value\\\": \\\"1\\\", \\\"sampleNumber\\\": 1}]\"},{\"finalResult\":\"[{\\\"id\\\": 533, \\\"side\\\": \\\"b\\\", \\\"value\\\": \\\"4\\\", \\\"sampleNumber\\\": 1}, {\\\"id\\\": 534, \\\"side\\\": \\\"b\\\", \\\"value\\\": \\\"4\\\", \\\"sampleNumber\\\": 2}]\"},{\"finalResult\":\"[{\\\"id\\\": 535, \\\"side\\\": \\\"c\\\", \\\"value\\\": \\\"3\\\", \\\"sampleNumber\\\": 1}, {\\\"id\\\": 536, \\\"side\\\": \\\"c\\\", \\\"value\\\": \\\"3\\\", \\\"sampleNumber\\\": 2}, {\\\"id\\\": 537, \\\"side\\\": \\\"c\\\", \\\"value\\\": \\\"3\\\", \\\"sampleNumber\\\": 3}, {\\\"id\\\": 538, \\\"side\\\": \\\"c\\\", \\\"value\\\": \\\"3\\\", \\\"sampleNumber\\\": 4}]\"}]";
        JSONArray jsonArray=new JSONArray(finalResult);
        int length=jsonArray.length();
        JSONArray side1JsonArray=length>0?new JSONArray(jsonArray.getJSONObject(0).getString("finalResult")):new JSONArray();
        for (int i = 0; i < side1JsonArray.length(); i++) {
            JSONObject jsonObject=side1JsonArray.getJSONObject(i);
            listA.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
        }
        JSONArray side2JsonArray=length>1?new JSONArray(jsonArray.getJSONObject(1).getString("finalResult")):new JSONArray();
        for (int i = 0; i < side2JsonArray.length(); i++) {
            JSONObject jsonObject=side2JsonArray.getJSONObject(i);
            listB.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
        }
        JSONArray side3JsonArray=length>2?new JSONArray(jsonArray.getJSONObject(2).getString("finalResult")):new JSONArray();
        for (int i = 0; i < side3JsonArray.length(); i++) {
            JSONObject jsonObject=side3JsonArray.getJSONObject(i);
            listC.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
        }
        JSONArray side4JsonArray=length>3?new JSONArray(jsonArray.getJSONObject(3).getString("finalResult")):new JSONArray();
        for (int i = 0; i < side4JsonArray.length(); i++) {
            JSONObject jsonObject=side4JsonArray.getJSONObject(i);
            listD.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
        }
        JSONArray side5JsonArray=length>4?new JSONArray(jsonArray.getJSONObject(4).getString("finalResult")):new JSONArray();
        for (int i = 0; i < side5JsonArray.length(); i++) {
            JSONObject jsonObject=side5JsonArray.getJSONObject(i);
            listE.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
        }
        JSONArray side6JsonArray=length>5?new JSONArray(jsonArray.getJSONObject(5).getString("finalResult")):new JSONArray();
        for (int i = 0; i < side6JsonArray.length(); i++) {
            JSONObject jsonObject=side6JsonArray.getJSONObject(i);
            listF.add(new Side(String.valueOf(jsonObject.get("sampleNumber")),jsonObject.getString("value"),jsonObject.getString("side")));
        }



/*
        Side side1 = new Side("1", "10", "A");
        Side side2 = new Side("2", "11", "A");
        Side side3 = new Side("3", "12", "A");
        Side side4 = new Side("4", "13", "A");
        listA.add(side1);
        listA.add(side2);
        listA.add(side3);
        listA.add(side4);
        Side side5 = new Side("1", "14", "B");
        Side side6 = new Side("2", "15", "B");
        listB.add(side5);
        listB.add(side6);
        Side side7 = new Side("1", "10", "C");
        Side side8 = new Side("2", "10", "C");
        listC.add(side7);
        listC.add(side8);
        listD.add(new Side("1", "11", "D"));
        listD.add(new Side("2", "22", "D"));

 */

        narrowestList.add(new Side("1","0.23","A"));
        narrowestList.add(new Side("2","1.11","B"));
        narrowestList.add(new Side("3","0.23","C"));
        narrowestList.add(new Side("4","1.11","D"));

        JSONArray preCOnd=new JSONArray("[{\n" +
                "\t\"id\": \"1\",\n" +
                "\t\"preConditionWeight\": \"10\"\n" +
                "},{\n" +
                "\t\"id\": \"2\",\n" +
                "\t\"preConditionWeight\": \"11\"\n" +
                "}\n" +
                "]");
        JSONArray postCOnd=new JSONArray("[{\n" +
                "\t\"id\": \"1\",\n" +
                "\t\"postConditionWeight\": \"10\"\n" +
                "},{\n" +
                "\t\"id\": \"2\",\n" +
                "\t\"postConditionWeight\": \"11\"\n" +
                "}\n" +
                "]");

        JSONArray torque=new JSONArray("[{\n" +
                "\t\"id\": \"1\",\n" +
                "\t\"goldBottleReading\": \"10\"\n" +
                "},{\n" +
                "\t\"id\": \"2\",\n" +
                "\t\"goldBottleReading\": \"11\"\n" +
                "}\n" +
                "]");

        JSONArray otherMagna=new JSONArray("[{\n" +
                "\t\"id\": \"1\",\n" +
                "\t\"value\": \"10\"\n" +
                "},{\n" +
                "\t\"id\": \"2\",\n" +
                "\t\"value\": \"11\"\n" +
                "}\n" +
                "]");

        JSONArray allVariable=new JSONArray("[{\"side\":\"B\",\"id\":1,\"value\":\"2314\"}]\n");


        processExcelReport(new File("C:\\Users\\chandipriya.g\\Desktop\\Edward\\Feasibility\\"+testCode+".xlsx"),allVariable,otherMagna,torque,preCOnd,postCOnd,testCaseNo,testItem,isNarrowest,testCode,sideForSample,listA, listB, listC, listD, listE, listF,narrowestList);


    }

    /**
     *  Process excel report based on testcode
     * @param file
     * @param allvariable
     * @param otherMagna
     * @param torque
     * @param preCOnd
     * @param postCOnd
     * @param testCaseNo
     * @param testItem
     * @param isNarrowest
     * @param testCode
     * @param sideForSample
     * @param listA
     * @param listB
     * @param listC
     * @param listD
     * @param listE
     * @param listF
     * @param narrowestList
     */
    private static void processExcelReport(File file,JSONArray allvariable,JSONArray otherMagna,JSONArray torque,JSONArray preCOnd, JSONArray postCOnd,String testCaseNo,String testItem,int isNarrowest,String testCode,int sideForSample,List<Side> listA, List<Side> listB, List<Side> listC, List<Side> listD, List<Side> listE, List<Side> listF,List<Side> narrowestList){
        if(testCode.equalsIgnoreCase("Seal Width Measurement") || testCode.equalsIgnoreCase("Peel Strength")) {
            // verify narrowest or not
            if(isNarrowest==0) {
                // Formation of excel for all seal and all peel
                excelAllSealFormation(file,testCaseNo,testItem,testCode,sideForSample, listA, listB, listC, listD, listE, listF);
            }else{
                // Formation of excel for norrowest seal and peel
                excelNarrowestSeal(file,testCaseNo,testItem,testCode,narrowestList,sideForSample);
            }
            // Formation of excel for lowpressure vaccum jars
        }else if(testCode.equalsIgnoreCase("Vacuum (Jars)")){
            excelLowPressure(file,testCaseNo,testItem,testCode,preCOnd,postCOnd);
        }// Formation of excel for torque
        else if(testCode.equalsIgnoreCase("Torque")){
            excelTorque(file,testCaseNo,testItem,testCode,torque);
        } // Formation of excel for Other test and Magna
        else if(testCode.equalsIgnoreCase("Other")|| testCode.equalsIgnoreCase("Magna-mike Wall Thickness Measurement")){
            excelOtherMagna(file,testCaseNo,testItem,testCode,otherMagna);
        }// Formation of excel for other variable test specifications
        else{
            excelAllvariable(file,testCaseNo,testItem,testCode,allvariable);
        }
    }

    /**
     * Excel formation for other variable test data forms
     * @param file
     * @param testCaseNo
     * @param testItem
     * @param testCode
     * @param allvariable
     */
    private static void excelAllvariable(File file,String testCaseNo,String testItem,String testCode,JSONArray allvariable){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet test = wb.createSheet(testCode);

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CENTER);
        cellStyle.setVerticalAlignment(CENTER);
        cellStyle.setWrapText(true);

        Row testNum = test.createRow(0);

        Cell testNumcell = testNum.createCell(0);
        testNumcell.setCellStyle(cellStyle);
        testNumcell.setCellValue("Test #");

        Cell testNumcellValue = testNum.createCell(1);
        testNumcellValue.setCellStyle(cellStyle);
        testNumcellValue.setCellValue(testCaseNo);

        Row testItemRow = test.createRow(1);

        Cell testItemcell = testItemRow.createCell(0);
        testItemcell.setCellStyle(cellStyle);
        testItemcell.setCellValue("Test Item");

        Cell testItemcellValue = testItemRow.createCell(1);
        testItemcellValue.setCellStyle(cellStyle);
        testItemcellValue.setCellValue(testItem);

        Row sideRow = test.createRow(3);

        Row headerRow = test.createRow(4);

        Cell cellA = sideRow.createCell(2);
        cellA.setCellValue(testCode);
        cellA.setCellStyle(cellStyle);
        test.addMergedRegion(new CellRangeAddress(3, 3, 2, 4));

        Cell headersample = headerRow.createCell(2);
        headersample.setCellStyle(cellStyle);
        headersample.setCellValue("Sample #");

        Cell headervalue = headerRow.createCell(3);
        headervalue.setCellStyle(cellStyle);
        headervalue.setCellValue("Value");

        Cell headerSide = headerRow.createCell(4);
        headerSide.setCellStyle(cellStyle);
        headerSide.setCellValue("Side");
        for (int i = 0; i < allvariable.length(); i++) {
            JSONObject jsonObject=allvariable.getJSONObject(i);
            Row row = test.createRow(i + 5);
                Cell sample = row.createCell(2);
                sample.setCellStyle(cellStyle);
                sample.setCellValue(String.valueOf(jsonObject.get("id")));

                Cell value = row.createCell(3);
                value.setCellStyle(cellStyle);
                value.setCellValue(jsonObject.getString("value"));

                Cell side = row.createCell(4);
                side.setCellStyle(cellStyle);
                side.setCellValue(jsonObject.getString("side"));
        }
        try  {
            wb.write(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Excel formation for other test  and magna
     * @param file
     * @param testCaseNo
     * @param testItem
     * @param testCode
     * @param otherMagna
     */
    private static void excelOtherMagna(File file,String testCaseNo,String testItem , String testCode, JSONArray otherMagna){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet test = wb.createSheet(testCode);

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CENTER);
        cellStyle.setVerticalAlignment(CENTER);
        cellStyle.setWrapText(true);

        Row testNum = test.createRow(0);

        Cell testNumcell = testNum.createCell(0);
        testNumcell.setCellStyle(cellStyle);
        testNumcell.setCellValue("Test #");

        Cell testNumcellValue = testNum.createCell(1);
        testNumcellValue.setCellStyle(cellStyle);
        testNumcellValue.setCellValue(testCaseNo);

        Row testItemRow = test.createRow(1);

        Cell testItemcell = testItemRow.createCell(0);
        testItemcell.setCellStyle(cellStyle);
        testItemcell.setCellValue("Test Item");

        Cell testItemcellValue = testItemRow.createCell(1);
        testItemcellValue.setCellStyle(cellStyle);
        testItemcellValue.setCellValue(testItem);

        Row sideRow = test.createRow(3);

        Row headerRow = test.createRow(4);

        Cell cellA = sideRow.createCell(2);
        cellA.setCellValue(testCode);
        cellA.setCellStyle(cellStyle);
        test.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));
        if(otherMagna.length()>0) {
            Cell headersample = headerRow.createCell(2);
            headersample.setCellStyle(cellStyle);
            headersample.setCellValue("Sample #");

            Cell headervalue = headerRow.createCell(3);
            headervalue.setCellStyle(cellStyle);
            headervalue.setCellValue("Value");
        }
        System.out.println("length "+otherMagna.length());
        for (int i = 0; i < otherMagna.length(); i++) {
            JSONObject jsonObject=otherMagna.getJSONObject(i);
            Row row = test.createRow(i + 5);

            Cell sample = row.createCell(2);
            sample.setCellStyle(cellStyle);
            sample.setCellValue(String.valueOf(jsonObject.get("id")));

            Cell value = row.createCell(3);
            value.setCellStyle(cellStyle);
            value.setCellValue(jsonObject.getString("value"));
        }
        try {
            wb.write(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Excel formation for torque
     * @param file
     * @param testCaseNo
     * @param testItem
     * @param testCode
     * @param torque
     */
    private static void excelTorque(File file,String testCaseNo,String testItem , String testCode, JSONArray torque){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet test = wb.createSheet(testCode);

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CENTER);
        cellStyle.setVerticalAlignment(CENTER);
        cellStyle.setWrapText(true);

        Row testNum = test.createRow(0);

        Cell testNumcell = testNum.createCell(0);
        testNumcell.setCellStyle(cellStyle);
        testNumcell.setCellValue("Test #");

        Cell testNumcellValue = testNum.createCell(1);
        testNumcellValue.setCellStyle(cellStyle);
        testNumcellValue.setCellValue(testCaseNo);

        Row testItemRow = test.createRow(1);

        Cell testItemcell = testItemRow.createCell(0);
        testItemcell.setCellStyle(cellStyle);
        testItemcell.setCellValue("Test Item");

        Cell testItemcellValue = testItemRow.createCell(1);
        testItemcellValue.setCellStyle(cellStyle);
        testItemcellValue.setCellValue(testItem);

        Row sideRow = test.createRow(3);

        Row headerRow = test.createRow(4);

        Cell cellA = sideRow.createCell(2);
        cellA.setCellValue(testCode);
        cellA.setCellStyle(cellStyle);
        test.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));

        Cell headersample = headerRow.createCell(2);
        headersample.setCellStyle(cellStyle);
        headersample.setCellValue("Sample #");

        Cell headervalue = headerRow.createCell(3);
        headervalue.setCellStyle(cellStyle);
        headervalue.setCellValue("Gold Bottle Dynamic Cal. Reading (lbFin)");

        for (int i = 0; i < torque.length(); i++) {
            JSONObject jsonObject=torque.getJSONObject(i);
            Row row = test.createRow(i + 5);

                Cell sample = row.createCell(2);
                sample.setCellStyle(cellStyle);
                sample.setCellValue(String.valueOf(jsonObject.get("id")));

                Cell value = row.createCell(3);
                value.setCellStyle(cellStyle);
                value.setCellValue(String.valueOf(jsonObject.get("goldBottleReading")));
        }
        try{
            wb.write(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Excel formation for low pressure
     * @param file
     * @param testCaseNo
     * @param testItem
     * @param testCode
     * @param preCond
     * @param postCond
     */
    private static void excelLowPressure(File file,String testCaseNo,String testItem , String testCode, JSONArray preCond,JSONArray postCond){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet test = wb.createSheet(testCode);

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CENTER);
        cellStyle.setVerticalAlignment(CENTER);
        cellStyle.setWrapText(true);

        Row testNum = test.createRow(0);

        Cell testNumcell = testNum.createCell(0);
        testNumcell.setCellStyle(cellStyle);
        testNumcell.setCellValue("Test #");

        Cell testNumcellValue = testNum.createCell(1);
        testNumcellValue.setCellStyle(cellStyle);
        testNumcellValue.setCellValue(testCaseNo);

        Row testItemRow = test.createRow(1);

        Cell testItemcell = testItemRow.createCell(0);
        testItemcell.setCellStyle(cellStyle);
        testItemcell.setCellValue("Test Item");

        Cell testItemcellValue = testItemRow.createCell(1);
        testItemcellValue.setCellStyle(cellStyle);
        testItemcellValue.setCellValue(testItem);

        Row sideRow = test.createRow(3);

        Row headerRow = test.createRow(4);

        Cell cellA = sideRow.createCell(2);
        cellA.setCellValue("Pre - Condition");
        cellA.setCellStyle(cellStyle);
        test.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));

        Cell headersample = headerRow.createCell(2);
        headersample.setCellStyle(cellStyle);
        headersample.setCellValue("Sample #");

        Cell headervalue = headerRow.createCell(3);
        headervalue.setCellStyle(cellStyle);
        headervalue.setCellValue("Pre -Cond Wt");

        Cell cellB = sideRow.createCell(5);
        cellB.setCellValue("Post - Condition");
        cellB.setCellStyle(cellStyle);
        test.addMergedRegion(new CellRangeAddress(3, 3, 5, 6));

        Cell headersampleB = headerRow.createCell(5);
        headersampleB.setCellStyle(cellStyle);
        headersampleB.setCellValue("Sample #");

        Cell headervalueB = headerRow.createCell(6);
        headervalueB.setCellStyle(cellStyle);
        headervalueB.setCellValue("Post -Cond Wt");

        int length =Math.max(preCond.length(), postCond.length());
        for (int i = 0; i < length; i++) {
            Row row = test.createRow(i + 5);
            if ( preCond.length() > i) {
                JSONObject jsonObject=preCond.getJSONObject(i);
                Cell sample = row.createCell(2);
                sample.setCellStyle(cellStyle);
                sample.setCellValue(String.valueOf(jsonObject.get("id")));

                Cell preCondValue = row.createCell(3);
                preCondValue.setCellStyle(cellStyle);
                preCondValue.setCellValue(jsonObject.getString("preConditionWeight"));

            }
            if ( postCond.length() > i) {
                JSONObject jsonObject=postCond.getJSONObject(i);
                Cell sample = row.createCell(5);
                sample.setCellStyle(cellStyle);
                sample.setCellValue(String.valueOf(jsonObject.get("id")));

                Cell preCondValue = row.createCell(6);
                preCondValue.setCellStyle(cellStyle);
                preCondValue.setCellValue(jsonObject.getString("postConditionWeight"));

            }
        }
        try{
            wb.write(new FileOutputStream(file));

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Excel formation for seal and peel test when narrowest is selected
     * @param file
     * @param testCaseNo
     * @param testItem
     * @param testCode
     * @param narrowestList
     * @param sideFormSample
     */
    private static void excelNarrowestSeal(File file,String testCaseNo,String testItem,String testCode,List<Side> narrowestList,int sideFormSample){
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet test ;
        if(testCode.equalsIgnoreCase("Peel Strength")){
            test= wb.createSheet("Narrowest peel");
        }else {
            test = wb.createSheet("Narrowest Seal");
        }

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CENTER);
        cellStyle.setVerticalAlignment(CENTER);
        cellStyle.setWrapText(true);

        Row testNum = test.createRow(0);

        Cell testNumcell = testNum.createCell(0);
        testNumcell.setCellStyle(cellStyle);
        testNumcell.setCellValue("Test #");

        Cell testNumcellValue = testNum.createCell(1);
        testNumcellValue.setCellStyle(cellStyle);
        testNumcellValue.setCellValue(testCaseNo);

        Row testItemRow = test.createRow(1);

        Cell testItemcell = testItemRow.createCell(0);
        testItemcell.setCellStyle(cellStyle);
        testItemcell.setCellValue("Test Item");

        Cell testItemcellValue = testItemRow.createCell(1);
        testItemcellValue.setCellStyle(cellStyle);
        testItemcellValue.setCellValue(testItem);

        Row sideRow = test.createRow(3);

        Row headerRow = test.createRow(4);

        Cell cellA = sideRow.createCell(2);
        cellA.setCellValue("Narrowest Seal");
        cellA.setCellStyle(cellStyle);
        test.addMergedRegion(new CellRangeAddress(3, 3, 2, 4));

        Cell headersample = headerRow.createCell(2);
        headersample.setCellStyle(cellStyle);
        headersample.setCellValue("Sample #");

        Cell headervalue = headerRow.createCell(3);
        headervalue.setCellStyle(cellStyle);
        headervalue.setCellValue("Value");

        Cell headerSide = headerRow.createCell(4);
        headerSide.setCellStyle(cellStyle);
        headerSide.setCellValue("Side");
        for (int i = 0; i < narrowestList.size(); i++) {
            Row row = test.createRow(i + 5);

                Cell sample = row.createCell(2);
                sample.setCellStyle(cellStyle);
                sample.setCellValue(Integer.parseInt(narrowestList.get(i).getId()));

                Cell value = row.createCell(3);
                value.setCellStyle(cellStyle);
                value.setCellValue(narrowestList.get(i).getValue());

                Cell side = row.createCell(4);
                side.setCellStyle(cellStyle);
                side.setCellValue(narrowestList.get(i).getSide());



        }
        try {
            wb.write(new FileOutputStream(file));

        } catch (IOException e) {
            e.printStackTrace();
        }




    }

    /**
     * Excel formation for seal and peel test when all seal selected
     * @param file
     * @param testCaseNo
     * @param testItem
     * @param testCode
     * @param sideForSample
     * @param listA
     * @param listB
     * @param listC
     * @param listD
     * @param listE
     * @param listF
     */
    private static void excelAllSealFormation(File file,String testCaseNo,String testItem,String testCode,int sideForSample,List<Side> listA, List<Side> listB, List<Side> listC, List<Side> listD, List<Side> listE, List<Side> listF) {
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet test;
        if(testCode.equalsIgnoreCase("Peel Strength")){
            test = wb.createSheet("All Peel");
        }else {
            test = wb.createSheet("All Seal");
        }

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CENTER);
        cellStyle.setVerticalAlignment(CENTER);
        cellStyle.setWrapText(true);

        Row testNum = test.createRow(0);

        Cell testNumcell = testNum.createCell(0);
        testNumcell.setCellStyle(cellStyle);
        testNumcell.setCellValue("Test #");

        Cell testNumcellValue = testNum.createCell(1);
        testNumcellValue.setCellStyle(cellStyle);
        testNumcellValue.setCellValue(testCaseNo);

        Row testItemRow = test.createRow(1);

        Cell testItemcell = testItemRow.createCell(0);
        testItemcell.setCellStyle(cellStyle);
        testItemcell.setCellValue("Test Item");

        Cell testItemcellValue = testItemRow.createCell(1);
        testItemcellValue.setCellStyle(cellStyle);
        testItemcellValue.setCellValue(testItem);

        Row sideRow = test.createRow(3);

        Row headerRow = test.createRow(4);

        if(sideForSample>=1 && listA.size()>0){

            Cell cellA = sideRow.createCell(2);
            cellA.setCellValue("Side "+listA.get(0).side);
            cellA.setCellStyle(cellStyle);
            test.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));

            Cell headersampleA = headerRow.createCell(2);
            headersampleA.setCellStyle(cellStyle);
            headersampleA.setCellValue("Sample #");

            Cell headervalueA = headerRow.createCell(3);
            headervalueA.setCellStyle(cellStyle);
            headervalueA.setCellValue("Value");
/*
        Cell headersideA = headerRow.createCell(2);
        headersideA.setCellValue("Side");

 */
        }
        if(sideForSample>=2 && listB.size()>0){

            Cell cellB = sideRow.createCell(5);
            cellB.setCellValue("Side "+listB.get(0).side);
            cellB.setCellStyle(cellStyle);
            test.addMergedRegion(new CellRangeAddress(3, 3, 5, 6));

            Cell headersampleB = headerRow.createCell(5);
            headersampleB.setCellStyle(cellStyle);
            headersampleB.setCellValue("Sample #");

            Cell headervalueB = headerRow.createCell(6);
            headervalueB.setCellStyle(cellStyle);
            headervalueB.setCellValue("Value");
/*
        Cell headersideB = headerRow.createCell(6);
        headersideB.setCellValue("Side");

 */
        }
        if(sideForSample>=3 && listC.size()>0){

            Cell cellC = sideRow.createCell(8);
            cellC.setCellValue("Side "+listC.get(0).side);
            cellC.setCellStyle(cellStyle);
            test.addMergedRegion(new CellRangeAddress(3, 3, 8, 9));

            Cell headerSampleC = headerRow.createCell(8);
            headerSampleC.setCellStyle(cellStyle);
            headerSampleC.setCellValue("Sample #");

            Cell headerValueC = headerRow.createCell(9);
            headerValueC.setCellStyle(cellStyle);
            headerValueC.setCellValue("Value");
/*
        Cell headerSideC= headerRow.createCell(2);
        headerSideC.setCellValue("Side");

 */
        }
        if (sideForSample>=4 && listD.size()>0) {


            Cell cellD = sideRow.createCell(11);
            cellD.setCellValue("Side "+listD.get(0).side);
            cellD.setCellStyle(cellStyle);
            test.addMergedRegion(new CellRangeAddress(3, 3, 11, 12));

            Cell headerSampleD = headerRow.createCell(11);
            headerSampleD.setCellStyle(cellStyle);
            headerSampleD.setCellValue("Sample #");

            Cell headerValueD = headerRow.createCell(12);
            headerValueD.setCellStyle(cellStyle);
            headerValueD.setCellValue("Value");
/*
        Cell headerSideD= headerRow.createCell(6);
        headerSideD.setCellValue("Side");

 */
        }
        if (sideForSample>=5 && listE.size()>0) {

            Cell cellE = sideRow.createCell(14);
            cellE.setCellValue("Side "+listE.get(0).side);
            cellE.setCellStyle(cellStyle);
            test.addMergedRegion(new CellRangeAddress(3, 3, 14, 15));

            Cell headerSampleE = headerRow.createCell(14);
            headerSampleE.setCellStyle(cellStyle);
            headerSampleE.setCellValue("Sample #");

            Cell headerValueE = headerRow.createCell(15);
            headerValueE.setCellStyle(cellStyle);
            headerValueE.setCellValue("Value");
        }
        if (sideForSample>=6 && listF.size()>0) {

            Cell cellF = sideRow.createCell(17);
            cellF.setCellValue("Side "+listF.get(0).side);
            cellF.setCellStyle(cellStyle);
            test.addMergedRegion(new CellRangeAddress(3, 3, 17, 18));

            Cell headerSampleF = headerRow.createCell(17);
            headerSampleF.setCellStyle(cellStyle);
            headerSampleF.setCellValue("Sample #");

            Cell headerValueF = headerRow.createCell(18);
            headerValueF.setCellStyle(cellStyle);
            headerValueF.setCellValue("Value");
        }


        int length = Math.max(Math.max(listA.size(), listB.size()), Math.max(listC.size(), listD.size()));
        length = Math.max(length, Math.max(listE.size(), listF.size()));
        for (int i = 0; i < length; i++) {
            Row row = test.createRow(i + 5);
            if (sideForSample>=1 && listA.size() > i) {
                Cell sampleA = row.createCell(2);
                sampleA.setCellStyle(cellStyle);
                sampleA.setCellValue(Integer.parseInt(listA.get(i).getId()));

                Cell valueA = row.createCell(3);
                valueA.setCellStyle(cellStyle);
                valueA.setCellValue(listA.get(i).getValue());
/*
                Cell sideA = row.createCell(2);
                sideA.setCellValue(listA.get(i).getSide());

 */
            }
            if (sideForSample>=2 &&listB.size() > i) {
                Cell sampleB = row.createCell(5);
                sampleB.setCellStyle(cellStyle);
                sampleB.setCellValue(Integer.parseInt(listB.get(i).getId()));

                Cell valueB = row.createCell(6);
                valueB.setCellStyle(cellStyle);
                valueB.setCellValue(listB.get(i).getValue());
/*
                Cell sideB = row.createCell(6);
                sideB.setCellValue(listB.get(i).getSide());

 */
            }
            if (sideForSample>=3 &&listC.size() > i) {
                Cell sampleC = row.createCell(8);
                sampleC.setCellStyle(cellStyle);
                sampleC.setCellValue(Integer.parseInt(listC.get(i).getId()));

                Cell valueC = row.createCell(9);
                valueC.setCellStyle(cellStyle);
                valueC.setCellValue(listC.get(i).getValue());
/*
                Cell sideC = row.createCell(2);
                sideC.setCellValue(listC.get(i).getSide());

 */
            }
            if (sideForSample>=4 && listD.size() > i) {

                Cell sampleD = row.createCell(11);
                sampleD.setCellStyle(cellStyle);
                sampleD.setCellValue(Integer.parseInt(listD.get(i).getId()));

                Cell valueD = row.createCell(12);
                valueD.setCellStyle(cellStyle);
                valueD.setCellValue(listD.get(i).getValue());
/*
                Cell sideD = row.createCell(6);
                sideD.setCellValue(listD.get(i).getSide());

 */
            }
            if (sideForSample>=5 &&listE.size() > i) {


                Cell sampleE = row.createCell(14);
                sampleE.setCellStyle(cellStyle);
                sampleE.setCellValue(Integer.parseInt(listE.get(i).getId()));

                Cell valueE = row.createCell(15);
                valueE.setCellStyle(cellStyle);
                valueE.setCellValue(listE.get(i).getValue());
/*
                Cell sideD = row.createCell(6);
                sideD.setCellValue(listD.get(i).getSide());

 */
            }
            if (sideForSample>=6 &&listF.size() > i) {


                Cell sampleF = row.createCell(17);
                sampleF.setCellStyle(cellStyle);
                sampleF.setCellValue(Integer.parseInt(listF.get(i).getId()));

                Cell valueF = row.createCell(18);
                valueF.setCellStyle(cellStyle);
                valueF.setCellValue(listF.get(i).getValue());
/*
                Cell sideD = row.createCell(6);
                sideD.setCellValue(listD.get(i).getSide());

 */
            }

        }


        try {
            wb.write(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
