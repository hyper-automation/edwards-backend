package main.java.com.edwards;

public class Side {
    String id;
    String value;
    String side;

    public Side(String id, String value, String side) {
        this.id = id;
        this.value = value;
        this.side = side;
    }
    public  Side(){

    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getSide() {
        return side;
    }
}
