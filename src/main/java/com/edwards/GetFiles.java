package main.java.com.edwards;

public class GetFiles {
    String fileName;
    String fileURL;
    String fileType;
    String fileTitle;

    public GetFiles(String fileTitle,String fileName, String fileURL, String fileType) {
        this.fileTitle=fileTitle;
        this.fileName = fileName;
        this.fileURL = fileURL;
        this.fileType = fileType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileURL() {
        return fileURL;
    }

    public String getFileType() {
        return fileType;
    }
}
