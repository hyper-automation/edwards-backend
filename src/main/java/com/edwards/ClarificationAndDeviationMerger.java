package main.java.com.edwards;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.quadwave.common.constant.AmperDataTypes;
import com.quadwave.common.exception.ErrorMessageException;
import com.quadwave.nbt.common.axpflow.command.SetWorkflowVariableCommand;
import com.quadwave.nbt.common.axpflow.service.Uploader;
import com.quadwave.nbt.common.execution.entity.WorkflowInstance;
import com.quadwave.nbt.common.execution.entity.WorkflowNode;
import com.quadwave.nbt.common.execution.entity.WorkflowVariable;
import com.quadwave.nbt.common.execution.java.JavaNodeHandler;
import com.quadwave.nbt.common.json.JacksonJsonUtil;
import com.quadwave.nbt.common.vo.DocumentInfoVo;
import com.quadwave.nbt.pojo.general.configuration.Dms;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ClarificationAndDeviationMerger extends JavaNodeHandler {
    @Override
    public void execute(WorkflowInstance workflowInstance, WorkflowNode workflowNode) {
        try {
            Dms defaultDmsName = executionContext.getGeneralService().getDmsService().findDefault(workflowInstance.getOrganizationId());
            List<WorkflowVariable> workflowVariables = new ArrayList<>();
            SetWorkflowVariableCommand setWorkflowVariableCommand = new SetWorkflowVariableCommand();
            WorkflowVariable finalDocWorkflow = new WorkflowVariable();
            WorkflowVariable data = new WorkflowVariable();
            List<InputStream> inputPdfList = new ArrayList<>();
            WorkflowVariable reportPath = workflowInstance.getVariableByName("reportPath");
            WorkflowVariable testDocvar = workflowInstance.getVariableByName("testdoc");
            if (testDocvar == null) {
                throw new ErrorMessageException(workflowNode.getName(), "variable " + testDocvar + " not-found");
            }
            extractDocInfo(testDocvar,inputPdfList);
            WorkflowVariable clarificationDocvar = workflowInstance.getVariableByName("clarificationdoc");
            if (clarificationDocvar == null) {
                throw new ErrorMessageException(workflowNode.getName(), "variable " + clarificationDocvar + " not-found");
            }
            String value=clarificationDocvar.getValue();
            extractDocInfo(clarificationDocvar,inputPdfList);
            String filepath=reportPath.getValue()+"clarification.pdf";
            System.out.println("*************filePath*************"+filepath);
            File finalFile=new File(filepath);
            mergePdfFiles(inputPdfList,new FileOutputStream(finalFile));
            System.out.println("--merge block completed -------------");
            Uploader doUploader = executionContext.getDocumentService().newUploader(workflowInstance.getOrganizationId());
            String uploadedFile = doUploader.uploadFile("/Org_"+workflowInstance.getOrganizationId()+"/report/",
                    filepath, defaultDmsName.getName());//dmspath , localpath , dms name;
            System.out.println("**********uploaded file path ***************"+uploadedFile);

            List<DocumentInfoVo> docInfo = new ArrayList<>();
            DocumentInfoVo documentInfoVo = new DocumentInfoVo();
            documentInfoVo.setUrl(uploadedFile);
            documentInfoVo.setName(finalFile.getName());
            documentInfoVo.setLocalPath(finalFile.getAbsolutePath());
            docInfo.add(documentInfoVo);

            String variableValue = JacksonJsonUtil.getInlineJson(docInfo);
            finalDocWorkflow.setDataType("Document");
            finalDocWorkflow.setValue(variableValue);
            finalDocWorkflow.setName("clarificationdoc");
            finalDocWorkflow.setCollection(false);
            data.setDataType(AmperDataTypes.STRING);
            data.setValue(value);
            data.setName("data");
            data.setCollection(false);
            setWorkflowVariableCommand.setInstanceId(workflowInstance.getId());
            workflowVariables.add(finalDocWorkflow);
            workflowVariables.add(data);


            setWorkflowVariableCommand.setWorkflowVariables(workflowVariables);
            setWorkflowVariableCommand.execute();

            System.out.println("-----------------test executed---------------");





        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void extractDocInfo(WorkflowVariable var , List<InputStream> inputStreams) throws FileNotFoundException {
        String varValue = var.getValue();
        System.out.println("document data ***************"+varValue);
        if (varValue != null && varValue.trim().length() > 0 && !(varValue.equals("[]"))) {
            System.out.println(var + varValue+"--------------");
            System.out.println("inside block get the "+var);
            DocumentInfoVo[] docInfos = JacksonJsonUtil.getObject(varValue, DocumentInfoVo[].class);
            System.out.println("-----------"+docInfos[0].getLocalPath()+"-------------------");
            for (int i=0 ; i <docInfos.length;i++) {
                File file = new File(docInfos[i].getLocalPath());
                try {
                    inputStreams.add(new FileInputStream(file));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

    }
    void mergePdfFiles(List<InputStream> inputPdfList,
                       OutputStream outputStream) throws Exception {

        System.out.println("_________merge block_______________________");
        //Create document and pdfReader objects.
        Document document = new Document();
        List<PdfReader> readers =
                new ArrayList<PdfReader>();
        int totalPages = 0;
        if(inputPdfList.size()>0) {
            //Create pdf Iterator object using inputPdfList.
            Iterator<InputStream> pdfIterator = inputPdfList.iterator();

            // Create reader list for the input pdf files.
            while (pdfIterator.hasNext()) {
                InputStream pdf = pdfIterator.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages = totalPages + pdfReader.getNumberOfPages();
            }

            // Create writer for the outputStream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);

            //Open document.
            document.open();

            //Contain the pdf data.
            PdfContentByte pageContentByte = writer.getDirectContent();

            PdfImportedPage pdfImportedPage;
            int currentPdfReaderPage = 1;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();

            // Iterate and process the reader list.
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                //Create page and add content.
                while (currentPdfReaderPage <= pdfReader.getNumberOfPages()) {
                    Rectangle r = pdfReader.getPageSize(
                            pdfReader.getPageN(currentPdfReaderPage ));
                    if(r.getWidth()>=792.0 && r.getHeight()>=612.0)
                        document.setPageSize(r);
                    else
                        document.setPageSize(PageSize.A4);

                    document.newPage();
                    pdfImportedPage = writer.getImportedPage(
                            pdfReader, currentPdfReaderPage);
                    pageContentByte.addTemplate(pdfImportedPage, 0,0);
                    currentPdfReaderPage++;
                }
                //          addPage(pdfReader, new ByteArrayOutputStream(),i,10);
                currentPdfReaderPage = 1;
            }



        }
        //Close document and outputStream.
        outputStream.flush();
        document.close();
        outputStream.close();
        System.out.println("Pdf files merged successfully.");
    }
}
