package main.java.com.edwards;

public enum WorkFlowEnum {
    worksheetvar,
    requestDocVar,
            C1Doc,
    C2Doc,
    conditionagingAAdoc,
    shipperDoc,
            drop1,
    Bridgeimpact,
            compression,
    vibrationdoc,
            load,
    lowpressuredoc,
            concentratedimpact,
    drop2,
            visulainspection,
    submerisionleakdoc,
            dpnonpous,
    porusdoc,
    peelopendoc,
    rfiddocument,
    Otherinspectiondoc,
    sealwidth,
            burst,
    foil,
            peelstrength,
    vaccum,
            torque,
    magna,
            other

    }
