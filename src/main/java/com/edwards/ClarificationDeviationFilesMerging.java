package main.java.com.edwards;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.quadwave.common.exception.ErrorMessageException;
import com.quadwave.nbt.common.axpflow.command.SetWorkflowVariableCommand;
import com.quadwave.nbt.common.axpflow.service.Downloader;
import com.quadwave.nbt.common.axpflow.service.Uploader;
import com.quadwave.nbt.common.execution.entity.WorkflowInstance;
import com.quadwave.nbt.common.execution.entity.WorkflowNode;
import com.quadwave.nbt.common.execution.entity.WorkflowVariable;
import com.quadwave.nbt.common.execution.java.JavaNodeHandler;
import com.quadwave.nbt.common.json.JacksonJsonUtil;
import com.quadwave.nbt.common.vo.DocumentInfoVo;
import com.quadwave.nbt.pojo.general.configuration.Dms;
import com.spire.xls.Worksheet;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ClarificationDeviationFilesMerging extends JavaNodeHandler {
    boolean textFlag=false;
    boolean xlflag=false;
    boolean imgflag=false;
    @Override
    public void execute(WorkflowInstance workflowInstance, WorkflowNode workflowNode) {
        try{
            List<WorkflowVariable> workflowVariables = new ArrayList<>();
            SetWorkflowVariableCommand setWorkflowVariableCommand = new SetWorkflowVariableCommand();
            WorkflowVariable finalDocWorkflow = new WorkflowVariable();
            WorkflowVariable filesVar = workflowInstance.getVariableByName("getFiles");
            WorkflowVariable reportPath = workflowInstance.getVariableByName("reportPath");
            WorkflowVariable testcodeVar=workflowInstance.getVariableByName("testcode");
            List<InputStream> inputPdfList = new ArrayList<>();
            String filePath=reportPath.getValue();
            if (filesVar == null || StringUtils.isBlank(filesVar.getValue())) {
                ErrorMessageException ex = new ErrorMessageException(workflowNode.getName(), "variable " + filesVar + " not-found");
                ex.printStackTrace();
                throw ex;
            }
            try {
                inputPdfList.add(new FileInputStream(filePath+"clarification.pdf"));
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
            JSONArray jsonArray=new JSONArray(filesVar.getValue());
            processListOfFiles(filePath,workflowInstance,jsonArray, inputPdfList);

            String fileListpath = filePath+testcodeVar.getValue()+"clarificationDeviation_FilesMerged.pdf";
            File pdfFile = new File(fileListpath);
            //Prepare output stream for merged pdf file.
            OutputStream outputStream =new FileOutputStream(pdfFile);
            Dms defaultDmsName = executionContext.getGeneralService().getDmsService().findDefault(workflowInstance.getOrganizationId());
            mergePdfFiles(filePath,inputPdfList, outputStream);
            System.out.println("-----merged------");
            List<File> fileList = new ArrayList<>();
            System.out.println(fileList);
            fileList.add(pdfFile);
            Uploader doUploader = executionContext.getDocumentService().newUploader(workflowInstance.getOrganizationId());
            String uploadedFile = doUploader.uploadFile("/Org_" + workflowInstance.getOrganizationId() + "/report/",
                    fileListpath, defaultDmsName.getName());//dmspath , localpath , dms name;
            System.out.println(uploadedFile);
            List<DocumentInfoVo> docInfo = new ArrayList<>();
            DocumentInfoVo documentInfoVo = new DocumentInfoVo();
            documentInfoVo.setUrl(uploadedFile);
            documentInfoVo.setName(pdfFile.getName());
            documentInfoVo.setLocalPath(pdfFile.getAbsolutePath());
            docInfo.add(documentInfoVo);

            String variableValue = JacksonJsonUtil.getInlineJson(docInfo);
            finalDocWorkflow.setDataType("Document");
            finalDocWorkflow.setValue(variableValue);
            finalDocWorkflow.setName("clarificationdoc");
            finalDocWorkflow.setCollection(false);

            //   _log.info("file path from ftp location" + result);


            setWorkflowVariableCommand.setInstanceId(workflowInstance.getId());
            workflowVariables.add(finalDocWorkflow);


            setWorkflowVariableCommand.setWorkflowVariables(workflowVariables);
            setWorkflowVariableCommand.execute();


        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void processListOfFiles(String filePath,WorkflowInstance workflowInstance,JSONArray filesTypeList, List<InputStream> pdfList) {
        try {
            File imgfile = new File(filePath+"img.pdf");
            File textfile = new File(filePath+"text.pdf");
            File excelfile = new File(filePath+"excel.pdf");
            System.out.println("******************************************************");
            FileOutputStream imgfileoutputStream = new FileOutputStream(imgfile);
            Dms defaultDmsName = executionContext.getGeneralService().getDmsService().findDefault(workflowInstance.getOrganizationId());
            Document imgdocument = new Document();
            PdfWriter.getInstance(imgdocument, imgfileoutputStream);
            imgdocument.open();
            Document textDocument = new Document();
            PdfWriter.getInstance(textDocument, new FileOutputStream(textfile));
            textDocument.open();
            Document iText_xls_2_pdf = new Document();
            PdfWriter.getInstance(iText_xls_2_pdf, new FileOutputStream(excelfile));
            iText_xls_2_pdf.open();

            for (Object obj : filesTypeList) {
                JSONObject jsonObject=new JSONObject(obj.toString());
                GetFiles filetype=new GetFiles(jsonObject.getString("fileTitle"),jsonObject.getString("fileName"),jsonObject.getString("fileURL"),jsonObject.getString("fileType") );
                Downloader downloader=executionContext.getDocumentService().newDownloader(workflowInstance.getOrganizationId());
                File downloadFile=downloader.downloadFile(filetype.getFileURL(),filePath+filetype.getFileName(), defaultDmsName.getName());
                String fileName=filetype.getFileName();
                String fileTitle=StringUtils.isEmpty(filetype.getFileTitle())?fileName:filetype.getFileTitle();
                String fileFormat = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
                System.out.println("****************************"+downloadFile.getAbsoluteFile());
                if (filetype.getFileType().equals("Photo")|| fileFormat.equalsIgnoreCase("jpg")||fileFormat.equalsIgnoreCase("jpeg")||fileFormat.equalsIgnoreCase("png")||fileFormat.equalsIgnoreCase("img")) {
                    try {
                        System.out.println("photo type");
                        imgdocument.newPage();
                        imgdocument.add(new Paragraph(fileTitle));
                        Image img = Image.getInstance(downloadFile.getAbsolutePath());
                        img.scaleToFit(PageSize.A5.getWidth(),PageSize.A5.getHeight());
                        System.out.println(img.getDpiX());
                        imgdocument.add(img);
                        System.out.println(imgdocument.getPageNumber()+"--------------------");
                        imgflag=true;
                    }catch (Exception e){
                        System.out.println("image exception-----"+e.getMessage());
                    }
                }
                if (filetype.getFileType().equals("Document")) {

                    if (fileFormat.equalsIgnoreCase("txt")) {
                        try {
                            textDocument.newPage();
                            System.out.println("text type");
                            FileInputStream inputStream=new FileInputStream(downloadFile);
                            InputStreamReader in = new InputStreamReader(inputStream);
                            BufferedReader bin = new BufferedReader(in);
                            String text;
                            while( (text =bin.readLine()) !=null) {
                                textDocument.add(new Paragraph(text));
                            }
                            textFlag=true;
                        }catch (Exception e){
                            System.out.println("text exception----------"+e.getMessage());
                        }
                    }
                    if (fileFormat.equalsIgnoreCase("doc")||fileFormat.equalsIgnoreCase("docx")) {
                        try {
                            String inputFile = downloadFile.getAbsolutePath();
                            String outputFile = filePath+"doc.pdf";
                            System.out.println("inputFile:" + inputFile + ",outputFile:" + outputFile);
                            FileInputStream in = new FileInputStream(downloadFile);
                            XWPFDocument document = new XWPFDocument(in);
                            File outFile = new File(outputFile);
                            OutputStream out = new FileOutputStream(outFile);
                            PdfOptions options = null;
                            PdfConverter.getInstance().convert(document, out, options);
                            try {
                                pdfList.add(new FileInputStream(outFile));
                            }catch (Exception e){
                                System.out.println("** document adding to list exception *******"+e.getMessage());
                            }
                        }catch (Exception e){
                            System.out.println(e.getStackTrace()+"_____________doc exception---------------");
                        }
                    }
                    if (fileFormat.equalsIgnoreCase("xls") || fileFormat.equalsIgnoreCase("xlsx")) {
                        try {
                            /*
                             * This code to read the whole excel data , as per the discussion with edwrad team they did not required to
                             * read the whole excel data , just they want to display the chart on the pdf.
                             */
                            iText_xls_2_pdf.newPage();
                            excelToPdf(iText_xls_2_pdf, fileFormat, downloadFile);
                            addChart(filePath,iText_xls_2_pdf,downloadFile);
                            xlflag=true;
                        }catch (Exception e){
                            System.out.println("excel exception ----"+e.getMessage());
                        }
                    }
                    if (fileFormat.equalsIgnoreCase("pdf")) {
                        try {
                            System.out.println("pdf type");
                            pdfList.add(new FileInputStream(downloadFile));
                        }catch (Exception e){
                            System.out.println("pdf excelption----"+e.getMessage());
                        }
                    }
                }
            }
            System.out.println(" adding files to stream list");
            try {
                if(textFlag) {
                    textDocument.close();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                if(xlflag) {

                    iText_xls_2_pdf.close();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                if(imgflag) {

                    imgdocument.close();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            System.out.println(pdfList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private static void excelToPdf(Document document, String fileFormat, File file)  {
        System.out.println("-------------excel to pdf------------------");
        try {
            if (fileFormat.equalsIgnoreCase("xls")) {
                System.out.println("-------------xls------------------");
                HSSFWorkbook my_xls_workbook = new HSSFWorkbook(new FileInputStream(file));
                HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
                Iterator<Row> rowIterator = my_worksheet.iterator();
                PdfPTable table = new PdfPTable(my_worksheet.getRow(2).getPhysicalNumberOfCells());
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        PdfPCell cell1 = null;
                        try {
                            cell1 = new PdfPCell(new Phrase(cell.getStringCellValue()));
                        } catch (Exception e) {
                            cell1 = new PdfPCell(new Phrase(String.valueOf(cell.getNumericCellValue())));

                        }
                        table.addCell(cell1);
                    }
                }
                document.add(table);
            }
            if (fileFormat.equalsIgnoreCase("xlsx")) {
                System.out.println("-------------xlsx------------------");
                XSSFWorkbook my_xls_workbook = new XSSFWorkbook(new FileInputStream(file));
                XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
                Iterator<Row> rowIterator = my_worksheet.iterator();
                PdfPTable table = new PdfPTable(my_worksheet.getRow(2).getPhysicalNumberOfCells());
                System.out.println("columns"+my_worksheet.getRow(2).getPhysicalNumberOfCells());
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        PdfPCell cell1 ;
                        try {
                            cell1 = new PdfPCell(new Phrase(cell.getStringCellValue()));
                        } catch (Exception e) {
                            cell1 = new PdfPCell(new Phrase(String.valueOf(cell.getNumericCellValue())));

                        }
                        table.addCell(cell1);
                    }
                }
                System.out.println("new----------"+table.toString());
                document.add(table);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    private static void addChart(String filePath,Document document,File file){
        try {
            com.spire.xls.Workbook workbook = new com.spire.xls.Workbook();
            workbook.loadFromFile(file.getAbsolutePath());
            for (int i = 0; i < workbook.getWorksheets().getCount(); i++) {
                if(("chart").equalsIgnoreCase(workbook.getWorksheets().get(i).getName())){
                    Worksheet worksheet=workbook.getWorksheets().get(i);
                    if(!(worksheet==null|| worksheet.isEmpty())) {
                        //Save the first chart in the first worksheet as image
                        BufferedImage image = workbook.saveChartAsImage(workbook.getWorksheets().get("Chart"), 0);
                        File imgFile = new File(filePath+"40CImg.png");
                        ImageIO.write(image, "png", imgFile);
                        document.newPage();
                        Image img = Image.getInstance(imgFile.getAbsolutePath());
                        img.scaleToFit(PageSize.A5.getWidth(),PageSize.A5.getHeight());
                        document.add(img);
                    }
                    break;
                }
            }




        }catch (Exception e){
            System.out.println("--------chart exception"+e.getMessage());
        }
    }
    private void mergePdfFiles(String filePath,List<InputStream> inputPdfList,
                               OutputStream outputStream) throws Exception {
        if(textFlag){
            try{
                System.out.println("******text**********");
                inputPdfList.add(new FileInputStream(filePath+"text.pdf"));
            }catch (Exception e ){
                System.out.println(e.getMessage());
            }
        }
        if(xlflag){
            try{
                System.out.println("**********xl*********");

                inputPdfList.add(new FileInputStream(filePath+"excel.pdf"));

            }catch (Exception e ){
                System.out.println(e.getMessage());
            }
        }
        if(imgflag){
            try{

                System.out.println("in image");
                inputPdfList.add(new FileInputStream(filePath+"img.pdf"));
            }catch (Exception e ){
                System.out.println(e.getMessage());
            }
        }
        System.out.println("_________merge block_______________________");
        //Create document and pdfReader objects.
        Document document = new Document();
        List<PdfReader> readers =
                new ArrayList<PdfReader>();
        int totalPages = 0;
        System.out.println(inputPdfList+"-----------------------");
        if(inputPdfList.size()>0) {
            //Create pdf Iterator object using inputPdfList.
            Iterator<InputStream> pdfIterator = inputPdfList.iterator();
            System.out.println("inside list 1");
            // Create reader list for the input pdf files.
            while (pdfIterator.hasNext()) {
                System.out.println("inside iterator");
                InputStream pdf = pdfIterator.next();
                PdfReader pdfReader = null;
                try {
                    pdfReader= new PdfReader(pdf);
                    readers.add(pdfReader);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
                System.out.println("_____________adding----------");
                totalPages = totalPages + pdfReader.getNumberOfPages();
                System.out.println("pages"+totalPages);
                pdf.close();
            }
            System.out.println("----------iterated-------------");
            // Create writer for the outputStream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);

            //Open document.
            document.open();

            //Contain the pdf data.
            PdfContentByte pageContentByte = writer.getDirectContent();

            PdfImportedPage pdfImportedPage;
            int currentPdfReaderPage = 1;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
            System.out.println("----------adding-------------");
            // Iterate and process the reader list.
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                //Create page and add content.
                while (currentPdfReaderPage <= pdfReader.getNumberOfPages()) {
                    Rectangle r = pdfReader.getPageSize(
                            pdfReader.getPageN(currentPdfReaderPage ));
                    if(r.getWidth()>=792.0 && r.getHeight()>=612.0)
                        document.setPageSize(r);
                    else
                        document.setPageSize(PageSize.A4);

                    document.newPage();
                    pdfImportedPage = writer.getImportedPage(
                            pdfReader, currentPdfReaderPage);
                    pageContentByte.addTemplate(pdfImportedPage, 0,0);
                    currentPdfReaderPage++;
                }
                //          addPage(pdfReader, new ByteArrayOutputStream(),i,10);
                currentPdfReaderPage = 1;
            }



        }
        //Close document and outputStream.
        outputStream.flush();
        document.close();
        outputStream.close();
        System.out.println("Pdf files merged successfully.");

    }
}
